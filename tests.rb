require './commands'
require 'tmpdir'
puts 'Running Tests'

# COMMANDS::GET_ALL_MYSTERIES
# development/assets/mystery Test1
mysteries = `#{COMMANDS::GET_ALL_MYSTERIES}`
puts mysteries
puts "COMMANDS::GET_ALL_MYSTERIES - #{mysteries == "mystery Test1\n"}"

# COMMANDS::GET_ALL_VIDEOS
# video1.avi
# video2.avi
# video3.avi
# video4.avi

videos = `#{COMMANDS::GET_ALL_VIDEOS.call('mystery Test1')}`
puts "COMMANDS::GET_ALL_VIDEOS - #{videos.split("\n").length == 5}"

# COMMANDS::GENERATE_RESULT_DIR
# development/assets/mystery Test1
# development/results/mystery Test1

result_dir = `#{COMMANDS::GENERATE_RESULT_DIR.call('mystery Test1')}`
puts "COMMANDS::GENERATE_RESULT_DIR - #{result_dir == "development/results/mystery Test1\n"}"

# COMMANDS::DOWNLOAD
# development/assets/mystery Test1/video1.avi

# tmpdir/video1.avi
# File.exists? downloaded_file == true

# COMMANDS::CONVERT
# tmpdir/video1.avi
# tmpdir/video1.mp4

Dir.mktmpdir do |dir|
    if system COMMANDS::DOWNLOAD.call('mystery Test1/video1.avi', dir)
        puts "COMMANDS::DOWNLOAD - #{File.exists? "#{dir}/video1.avi"}"
    end
    if system COMMANDS::CONVERT.call("#{dir}/video1.avi", "#{dir}/video1.mp4")
        puts "COMMANDS::CONVERT - #{File.exists? "#{dir}/video1.mp4"}"
    end
end


# COMMANDS::UPLOAD
# tmpdir/video1.mp4
# development/results/mystery Test1/video1.mp4