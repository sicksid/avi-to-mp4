require 'dotenv/load'

module COMMANDS
  LOGIN = [
    "mega-login",
    ENV['MEGA_EMAIL'],
    ENV['MEGA_PASSWORD']
  ].join(' ')
  
  DOWNLOAD = -> (path, directory) {
    command = [
      "mega-get",
      "\"#{ENV['ENVIRONMENT']}/assets/#{path}\"",
      "\"#{directory}\""
    ].join(' ')
    puts command
    command
  }

  UPLOAD = -> (localfile, dstremotepath) {
    command = [
      "mega-put",
      "-c",
      localfile,
      dstremotepath
    ].join(' ')
    puts command
    command
  }

  GENERATE_RESULT_DIR = -> (directory) {
    command = "echo #{ENV['ENVIRONMENT']}/results/#{directory}"
    puts command
    command
  }

  CONVERT = -> (input, output) {
    command = [
      "ffmpeg",
      "-y",
      "-i",
      input,
      "-crf",
      "23",
      "-preset",
      "medium",
      "-movflags",
      "+faststart",
      "-strict",
      "-2",
      output  
    ].join(' ')
    puts command
    command
  }

  GET_ALL_MYSTERIES = [
    "mega-ls",
    "#{ENV['ENVIRONMENT']}/assets",
  ].join(' ')

  GET_ALL_VIDEOS = -> (mystery_dir) {
    command = [
      "mega-ls",
      "\"#{ENV['ENVIRONMENT']}/assets/#{mystery_dir}\""
    ].join(' ')
    puts command
    command
  }
end
