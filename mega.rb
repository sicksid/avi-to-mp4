# here we are gonna pull the files
require './commands'
require 'tmpdir'

system COMMANDS::LOGIN
folders = `#{COMMANDS::GET_ALL_MYSTERIES}`
folders.split("\n").each do |folder|
  puts "Processing #{folder}"

  videos = `#{COMMANDS::GET_ALL_VIDEOS.call(folder)}`
  puts "Listing videos: \n #{videos}"
  result_folder = `#{COMMANDS::GENERATE_RESULT_DIR.call(folder.gsub("\n", ""))}`
  videos.split("\n").each_with_index do |video, index|
    unless index == 0
      Dir.mktmpdir do |directory|
        system "#{COMMANDS::DOWNLOAD.call("#{folder}/#{video}", directory)}"
        input_path = "\"#{directory}/#{video}\""
        output_file = "#{video.split('.').first}.mp4"
        output_path = "\"#{directory}/#{output_file}\""

        if system COMMANDS::CONVERT.call(input_path, output_path)
          puts "Converting #{video} to mp4"
        end
        
        if system COMMANDS::UPLOAD.call(output_path, "\"#{result_folder.gsub("\n", "")}/#{output_file}\"")
          puts "Finished Processing #{video} for #{folder}"
        end
      end
    end
  end
end
